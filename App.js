import React from "react";

import { AuthProvider } from "./src/utils/auth";
import { RootNavigation } from "./src/navigation/RootNavigation";
import { SafeAreaProvider } from "react-native-safe-area-context";

import Amplify from "aws-amplify";
import config from "./src/aws-exports";
import { ShopProvider } from "./src/utils/shop";

Amplify.configure(config);

function App() {
   return (
      <AuthProvider>
         <ShopProvider>
            <SafeAreaProvider>
               <RootNavigation />
            </SafeAreaProvider>
         </ShopProvider>
      </AuthProvider>
   );
}

export default App;
