import React from "react";
import { createNativeStackNavigator } from "@react-navigation/native-stack";

import Home from "../screens/Home";
import Details from "../screens/Details";

const Stack = createNativeStackNavigator();

function UserNavigation() {
   return (
      <Stack.Navigator screenOptions={{ headerShown: false }}>
         <Stack.Screen name="Home" component={Home} />
         <Stack.Screen name="DetailsScreen" component={Details} />
      </Stack.Navigator>
   );
}

export { UserNavigation };
