import React from "react";
import { StatusBar } from "react-native";
import { NavigationContainer } from "@react-navigation/native";
import { createNativeStackNavigator } from "@react-navigation/native-stack";

import COLORS from "../consts/colors";

import { useAuth } from "../utils/auth";
import { AuthNavigation } from "./AuthNavigation";
import { UserNavigation } from "./UserNavigation";

import Cart from "../screens/Cart";
import SignIn from "../screens/SignIn";
import SignUp from "../screens/SignUp";
import Home from "../screens/Home";
import ForgotPassword from "../screens/ForgotPassword";
import Details from "../screens/Details";
import ConfirmScreen from "../screens/ConfirmScreen";

const Stack = createNativeStackNavigator();

function RootNavigation() {
   let auth = useAuth();

   return (
      <NavigationContainer>
         <StatusBar backgroundColor={COLORS.white} barStyle="dark-content" />
         <Stack.Navigator initialRouteName="Home" screenOptions={{ headerShown: false }}>
            {!!auth.user ? (
               <>
                  <Stack.Screen name="Cart" component={Cart} />
               </>
            ) : (
               <>
                  <Stack.Screen name="SignIn" component={SignIn} />
                  <Stack.Screen name="SignUp" component={SignUp} />
                  <Stack.Screen name="ForgotPassword" component={ForgotPassword} />
                  <Stack.Screen name="ConfirmScreen" component={ConfirmScreen} />
               </>
            )}

            <Stack.Screen name="Home" component={Home} />
            <Stack.Screen name="DetailsScreen" component={Details} />
         </Stack.Navigator>
         {/* {!!auth.user ? <UserNavigation /> : <AuthNavigation />} */}
      </NavigationContainer>
   );
}

export { RootNavigation };
