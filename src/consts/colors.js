const COLORS = {
   white: "#FFF",
   dark: "#000",
   primary: "#C78E2E",
   secondary: "#F5D7A4",
   light: "#f9f9f9",
   grey: "#908e8c",
   orange: "#f5a623",
};

export default COLORS;
