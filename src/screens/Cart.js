import React from "react";
import { View, Text, FlatList, TouchableOpacity, StyleSheet, Image, Alert } from "react-native";
import { useShop } from "../utils/shop";
import { SafeAreaView } from "react-native-safe-area-context";
import { MaterialIcons as Icon } from "react-native-vector-icons";
import COLORS from "../consts/colors";

const Cart = ({ navigation }) => {
   let { cart, total, makePayment } = useShop();

   function goBack() {
      navigation.goBack();
   }

   function onSubmit() {
      Alert.alert("Pago realizado", "Compra hecha con exito", [
         {
            text: "OK",
            onPress: () => {
               makePayment();
               navigation.navigate("Home");
            },
         },
      ]);
   }

   return (
      <SafeAreaView style={{ flex: 1, backgroundColor: COLORS.white }}>
         <View style={styles.headerContainer}>
            <TouchableOpacity style={{ marginHorizontal: 10 }} onPress={goBack}>
               <Icon size={28} color={COLORS.primary} name="arrow-back-ios" />
            </TouchableOpacity>

            <Text style={{ fontSize: 25, fontWeight: "bold" }}>Resumen de pedido</Text>
         </View>
         {!!cart.length ? (
            <FlatList
               data={cart}
               renderItem={Item}
               contentContainerStyle={{ paddingHorizontal: 15 }}
               keyExtractor={(item, index) => `product-${index}`}
            />
         ) : (
            <View style={{ flex: 1, alignItems: "center", justifyContent: "center" }}>
               <Text style={{ fontSize: 20, color: COLORS.grey }}>
                  No hay elementos seleccionados
               </Text>
            </View>
         )}
         <Text style={{ textAlign: "right", fontSize: 25, paddingHorizontal: 20 }}>
            <Text style={{ fontWeight: "bold" }}>Total:</Text>
            <Text> S./ {total.toFixed(2)}</Text>
         </Text>
         <View style={{ padding: 20 }}>
            <TouchableOpacity style={styles.submitBtn} onPress={onSubmit}>
               <Text style={styles.submitText}>Pagar </Text>
               <Icon size={30} color={COLORS.white} name="credit-card" />
            </TouchableOpacity>
         </View>
      </SafeAreaView>
   );
};

const Item = ({ item }) => (
   <View style={styles.itemContainer}>
      <Image
         source={item.image}
         style={{ height: 80, width: 80, marginRight: 8, borderRadius: 5 }}
      />
      <View style={{ justifyContent: "center" }}>
         <Text style={{ fontSize: 20 }}>{item.name}</Text>
         <Text style={{ fontSize: 25 }}>S./ {item.price}</Text>
      </View>
   </View>
);

const styles = StyleSheet.create({
   headerContainer: {
      paddingVertical: 8,
      flexDirection: "row",
      alignItems: "center",
   },
   itemContainer: {
      padding: 8,
      marginBottom: 8,
      borderRadius: 15,
      borderWidth: 1,
      borderColor: "rgba(0,0,0,0.2)",
      flexDirection: "row",
   },
   submitBtn: {
      padding: 12,
      borderRadius: 10,
      alignItems: "center",
      flexDirection: "row",
      justifyContent: "center",
      backgroundColor: COLORS.primary,
   },
   submitText: {
      fontSize: 20,
      color: COLORS.white,
   },
});

export default Cart;
