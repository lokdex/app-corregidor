import React, { useState } from "react";
import {
   Dimensions,
   FlatList,
   SafeAreaView,
   ScrollView,
   StatusBar,
   ImageBackground,
   StyleSheet,
   Text,
   TextInput,
   TouchableOpacity,
   View,
   Image,
   Animated,
   Alert,
} from "react-native";
import { useAuth } from "../utils/auth";

import Icon from "react-native-vector-icons/MaterialIcons";
import COLORS from "../consts/colors";
import habitaciones from "../consts/habitaciones";
import comidas from "../consts/comidas";
import resocial from "../assets/resocial.jpg";
import arequipa from "../assets/Aqp.jpg";

const { width } = Dimensions.get("screen");
const cardWidth = width / 1.6;

const Home = ({ navigation }) => {
   const categories = ["Habitaciones", "Comidas", "Lugares Turísticos", "Responsabilidad Social"];
   const [selectedCategoryIndex, setSelectedCategoryIndex] = useState(0);
   const [activeCardIndex, setActiveCardIndex] = useState(0);
   const scrollX = React.useRef(new Animated.Value(0)).current;
   const { user, signOut } = useAuth();

   async function onSignOut() {
      try {
         await signOut();
         Alert.alert("Sign Out", "Sesion finalizada");
      } catch {
         Alert.alert("Error", "no se pudo cerrar la sesión");
      }
   }

   const CategoryList = ({ navigation }) => {
      return (
         <View style={style.categoryListContainer}>
            <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
               {categories.map((item, index) => (
                  <TouchableOpacity
                     key={index}
                     activeOpacity={0.8}
                     onPress={() => setSelectedCategoryIndex(index)}
                  >
                     <View>
                        <Text
                           style={{
                              ...style.categoryListText,
                              color: selectedCategoryIndex == index ? COLORS.primary : COLORS.grey,
                           }}
                        >
                           {item}
                        </Text>
                        {selectedCategoryIndex == index && (
                           <View
                              style={{
                                 height: 3,
                                 width: 30,
                                 backgroundColor: COLORS.primary,
                                 marginTop: 2,
                              }}
                           />
                        )}
                     </View>
                  </TouchableOpacity>
               ))}
            </ScrollView>
         </View>
      );
   };
   const Card = ({ hotel, index }) => {
      const inputRange = [(index - 1) * cardWidth, index * cardWidth, (index + 1) * cardWidth];
      const opacity = scrollX.interpolate({
         inputRange,
         outputRange: [0.7, 0, 0.7],
      });
      const scale = scrollX.interpolate({
         inputRange,
         outputRange: [0.8, 1, 0.8],
      });
      return (
         <TouchableOpacity
            disabled={activeCardIndex != index}
            activeOpacity={1}
            onPress={() => navigation.navigate("DetailsScreen", hotel)}
         >
            <Animated.View style={{ ...style.card, transform: [{ scale }] }}>
               <Animated.View style={{ ...style.cardOverLay, opacity }} />
               <View style={style.priceTag}>
                  <Text style={{ color: COLORS.white, fontSize: 20, fontWeight: "bold" }}>
                     S/. {hotel.price}
                  </Text>
               </View>
               <Image source={hotel.image} style={style.cardImage} />
               <View style={style.cardDetails}>
                  <View style={{ flexDirection: "row", justifyContent: "space-between" }}>
                     <View>
                        <Text style={{ fontWeight: "bold", fontSize: 17 }}>{hotel.name}</Text>
                     </View>
                  </View>
                  <View
                     style={{
                        flexDirection: "row",
                        justifyContent: "space-between",
                        marginTop: 10,
                     }}
                  >
                     <View style={{ flexDirection: "row" }}>
                        <Icon name="star" size={15} color={COLORS.orange} />
                        <Icon name="star" size={15} color={COLORS.orange} />
                        <Icon name="star" size={15} color={COLORS.orange} />
                        <Icon name="star" size={15} color={COLORS.orange} />
                        <Icon name="star" size={15} color={COLORS.grey} />
                     </View>
                     <Text style={{ fontSize: 10, color: COLORS.grey }}>365reviews</Text>
                  </View>
               </View>
            </Animated.View>
         </TouchableOpacity>
      );
   };
   const TopHotelCard = ({ hotel }) => {
      return (
         <View style={style.topHotelCard}>
            <View
               style={{
                  position: "absolute",
                  top: 5,
                  right: 5,
                  zIndex: 1,
                  flexDirection: "row",
               }}
            >
               <Icon name="star" size={15} color={COLORS.orange} />
               <Text style={{ color: COLORS.white, fontWeight: "bold", fontSize: 15 }}>5.0</Text>
            </View>
            <Image style={style.topHotelCardImage} source={hotel.image} />
            <View style={{ paddingVertical: 5, paddingHorizontal: 10 }}>
               <Text style={{ fontSize: 10, fontWeight: "bold" }}>{hotel.name}</Text>
            </View>
         </View>
      );
   };

   return (
      <SafeAreaView style={{ flex: 1, backgroundColor: COLORS.white }}>
         <View style={[style.header, { alignItems: "center" }]}>
            <View style={{ paddingBottom: 15 }}>
               <Text style={{ fontSize: 30, fontWeight: "bold" }}>Gestiona tu estadía</Text>
               <View style={{ flexDirection: "row" }}>
                  <Text style={{ fontSize: 30, fontWeight: "bold" }}>en </Text>
                  <Text style={{ fontSize: 30, fontWeight: "bold", color: COLORS.primary }}>
                     El Corregidor
                  </Text>
               </View>
            </View>
            {user ? (
               <TouchableOpacity onPress={onSignOut}>
                  <Icon name="logout" size={38} color={COLORS.grey} />
               </TouchableOpacity>
            ) : (
               <TouchableOpacity onPress={() => navigation.navigate("SignIn")}>
                  <Icon name="person-outline" size={38} color={COLORS.grey} />
               </TouchableOpacity>
            )}
         </View>
         <ScrollView showsVerticalScrollIndicator={false}>
            {!!user && (
               <TouchableOpacity
                  style={{
                     padding: 5,
                     borderRadius: 20,
                     marginHorizontal: 10,
                     flexDirection: "row",
                     alignItems: "center",
                     justifyContent: "center",
                     backgroundColor: COLORS.primary,
                  }}
                  onPress={() => navigation.navigate("Cart")}
               >
                  <Icon
                     name="shopping-cart"
                     size={30}
                     style={{ marginLeft: 20 }}
                     color={COLORS.white}
                  />
                  <Text style={{ color: COLORS.white }}>Carrito</Text>
               </TouchableOpacity>
            )}
            <CategoryList />
            {selectedCategoryIndex === 0 ? (
               <>
                  <View>
                     <Animated.FlatList
                        onMomentumScrollEnd={(e) => {
                           setActiveCardIndex(
                              Math.round(e.nativeEvent.contentOffset.x / cardWidth)
                           );
                        }}
                        onScroll={Animated.event(
                           [{ nativeEvent: { contentOffset: { x: scrollX } } }],
                           {
                              useNativeDriver: true,
                           }
                        )}
                        horizontal
                        data={habitaciones}
                        keyExtractor={(item) => item.id}
                        contentContainerStyle={{
                           paddingVertical: 30,
                           paddingLeft: 20,
                           paddingRight: cardWidth / 2 - 40,
                        }}
                        showsHorizontalScrollIndicator={false}
                        renderItem={({ item, index }) => <Card hotel={item} index={index} />}
                        snapToInterval={cardWidth}
                     />
                  </View>
                  <View
                     style={{
                        flexDirection: "row",
                        justifyContent: "space-between",
                        marginHorizontal: 20,
                     }}
                  >
                     <Text style={{ fontWeight: "bold", color: COLORS.grey }}>
                        Top habitaciones
                     </Text>
                  </View>
                  <FlatList
                     data={habitaciones}
                     horizontal
                     showsHorizontalScrollIndicator={false}
                     contentContainerStyle={{
                        paddingLeft: 20,
                        marginTop: 20,
                        paddingBottom: 30,
                     }}
                     renderItem={({ item }) => <TopHotelCard hotel={item} />}
                  />
               </>
            ) : selectedCategoryIndex === 1 ? (
               <>
                  <View>
                     <Animated.FlatList
                        onMomentumScrollEnd={(e) => {
                           setActiveCardIndex(
                              Math.round(e.nativeEvent.contentOffset.x / cardWidth)
                           );
                        }}
                        onScroll={Animated.event(
                           [{ nativeEvent: { contentOffset: { x: scrollX } } }],
                           {
                              useNativeDriver: true,
                           }
                        )}
                        horizontal
                        data={comidas}
                        keyExtractor={(item) => item.id}
                        contentContainerStyle={{
                           paddingVertical: 30,
                           paddingLeft: 20,
                           paddingRight: cardWidth / 2 - 40,
                        }}
                        showsHorizontalScrollIndicator={false}
                        renderItem={({ item, index }) => <Card hotel={item} index={index} />}
                        snapToInterval={cardWidth}
                     />
                  </View>
                  <View
                     style={{
                        flexDirection: "row",
                        justifyContent: "space-between",
                        marginHorizontal: 20,
                     }}
                  >
                     <Text style={{ fontWeight: "bold", color: COLORS.grey }}>
                        Comidas destacadas
                     </Text>
                  </View>
                  <FlatList
                     data={comidas}
                     horizontal
                     showsHorizontalScrollIndicator={false}
                     contentContainerStyle={{
                        paddingLeft: 20,
                        marginTop: 20,
                        paddingBottom: 30,
                     }}
                     renderItem={({ item }) => <TopHotelCard hotel={item} />}
                  />
               </>
            ) : selectedCategoryIndex === 2 ? (
               <>
                  <ScrollView
                     showsVerticalScrollIndicator={false}
                     contentContainerStyle={{
                        backgroundColor: COLORS.white,
                        padding: 20,
                     }}
                  >
                     <ImageBackground style={style.headerImage} source={arequipa} />
                     <View>
                        <View style={{ marginTop: 20, paddingHorizontal: 20 }}>
                           <Text style={{ fontSize: 20, fontWeight: "bold" }}>Arequipa</Text>

                           <View style={{ marginTop: 20 }}>
                              <Text
                                 style={{
                                    lineHeight: 20,
                                    color: COLORS.grey,
                                    fontSize: 16,
                                    textAlign: "justify",
                                 }}
                              >
                                 Historia, paisajes fabulosos, vida silvestre, aventura,
                                 gastronomía, arte y mucho más se esconde en nuestra maravillosa
                                 ciudad custodiada firmemente por el imponente volcán Misti. Podrás
                                 visitar innumerables casonas coloniales de corte europeo-andina,
                                 que están adornadas también por bóvedas y arcos blancos hechos de
                                 sillar, además de incontables picanterías con variadas alternativas
                                 para saciar tus caprichos alimenticios. Para complementar la
                                 aventura y tras contar con el estómago satisfecho, Arequipa también
                                 tiene sectores para estimular tu intelecto y hambre de
                                 conocimiento. Se trata de santuarios arquitectónicos y
                                 arqueológicos tan místicos como milenarios.
                              </Text>
                           </View>
                        </View>
                     </View>
                  </ScrollView>
               </>
            ) : (
               <>
                  <ScrollView
                     showsVerticalScrollIndicator={false}
                     contentContainerStyle={{
                        backgroundColor: COLORS.white,
                        padding: 20,
                     }}
                  >
                     <ImageBackground style={style.headerImage} source={resocial} />
                     <View>
                        <View style={{ marginTop: 20, paddingHorizontal: 20 }}>
                           <Text style={{ fontSize: 20, fontWeight: "bold" }}>
                              EXPLOTACIÓN SEXUAL INFANTIL Y MALTRATO A LA MUJER
                           </Text>

                           <View style={{ marginTop: 20 }}>
                              <Text
                                 style={{
                                    lineHeight: 20,
                                    color: COLORS.grey,
                                    fontSize: 16,
                                    textAlign: "justify",
                                 }}
                              >
                                 En Grupo Cassana estamos comprometidos con una política de
                                 tolerancia cero respecto a la explotación sexual infantil y al
                                 maltrato a la mujer, actuando activamente en la capacitación y
                                 asistencia continua de mujeres y niños, logrando de este modo
                                 prevenir futuros casos de violencia.
                              </Text>
                           </View>
                        </View>
                     </View>
                  </ScrollView>
               </>
            )}
         </ScrollView>
      </SafeAreaView>
   );
};

const style = StyleSheet.create({
   header: {
      marginTop: 20,
      flexDirection: "row",
      justifyContent: "space-between",
      paddingHorizontal: 20,
   },
   headerImage: {
      height: 240,
      borderRadius: 40,
      overflow: "hidden",
   },
   searchInputContainer: {
      height: 50,
      backgroundColor: COLORS.light,
      marginTop: 15,
      marginLeft: 20,
      borderTopLeftRadius: 30,
      borderBottomLeftRadius: 30,
      flexDirection: "row",
      alignItems: "center",
   },
   categoryListContainer: {
      flexDirection: "row",
      justifyContent: "space-between",
      marginHorizontal: 20,
      marginTop: 30,
   },
   categoryListText: {
      fontSize: 17,
      fontWeight: "bold",
      padding: 4,
      margin: 4,
   },
   card: {
      height: 280,
      width: cardWidth,
      elevation: 15,
      marginRight: 20,
      borderRadius: 15,
      backgroundColor: COLORS.white,
   },
   cardImage: {
      height: 200,
      width: "100%",
      borderTopLeftRadius: 15,
      borderTopRightRadius: 15,
   },
   priceTag: {
      height: 60,
      width: 80,
      backgroundColor: COLORS.primary,
      position: "absolute",
      zIndex: 1,
      right: 0,
      borderTopRightRadius: 15,
      borderBottomLeftRadius: 15,
      justifyContent: "center",
      alignItems: "center",
   },
   cardDetails: {
      height: 100,
      borderRadius: 16,
      backgroundColor: COLORS.white,
      position: "absolute",
      bottom: 0,
      padding: 16,
      width: "100%",
   },
   cardOverLay: {
      height: 280,
      backgroundColor: COLORS.white,
      position: "absolute",
      zIndex: 100,
      width: cardWidth,
      borderRadius: 15,
   },
   topHotelCard: {
      height: 120,
      width: 120,
      backgroundColor: COLORS.white,
      elevation: 15,
      marginHorizontal: 10,
      borderRadius: 10,
   },
   topHotelCardImage: {
      height: 80,
      width: "100%",
      borderTopRightRadius: 10,
      borderTopLeftRadius: 10,
   },
});

export default Home;
