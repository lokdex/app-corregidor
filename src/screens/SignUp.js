import React, { useState } from "react";
import { StyleSheet, Text, View, Image, TextInput, Button, TouchableOpacity } from "react-native";
import COLORS from "../consts/colors";
import { useAuth } from "../utils/auth";

import logo from "../assets/logoCorregidor.png";

export default function SignUp({ navigation }) {
   const { signUp } = useAuth();
   const [email, setEmail] = useState("");
   const [password, setPassword] = useState("");

   function navigateSignIn() {
      navigation.navigate("SignIn");
   }

   async function handleSignUp() {
      try {
         console.log("dude");
         const formData = {
            username: email,
            password: password,
         };

         await signUp(formData);
         navigation.navigate("ConfirmScreen");
      } catch (error) {
         console.log(error);
      }
   }

   return (
      <View style={styles.container}>
         <Image style={styles.image} source={logo} />
         <View style={styles.inputView}>
            <TextInput
               style={styles.TextInput}
               placeholder="Email"
               placeholderTextColor={COLORS.grey}
               onChangeText={setEmail}
            />
         </View>
         <View style={styles.inputView}>
            <TextInput
               style={styles.TextInput}
               placeholder="Password"
               placeholderTextColor={COLORS.grey}
               secureTextEntry={true}
               onChangeText={setPassword}
            />
         </View>
         <TouchableOpacity style={styles.loginBtn} onPress={handleSignUp}>
            <Text style={styles.loginText}>REGISTRARSE</Text>
         </TouchableOpacity>
         <TouchableOpacity onPress={navigateSignIn}>
            <Text style={styles.forgot_button}>Ya tengo una cuenta</Text>
         </TouchableOpacity>
      </View>
   );
}

const styles = StyleSheet.create({
   container: {
      flex: 1,
      backgroundColor: "#fff",
      alignItems: "center",
      justifyContent: "center",
   },

   image: {
      width: 280,
      height: 240,
      marginBottom: 40,
   },

   inputView: {
      backgroundColor: COLORS.white,
      borderRadius: 30,
      borderColor: COLORS.grey,
      borderWidth: 1,
      width: "70%",
      height: 45,
      marginBottom: 20,
   },

   TextInput: {
      height: 50,
      flex: 1,
      padding: 10,
      marginLeft: 20,
   },

   forgot_button: {
      height: 30,
      marginTop: 40,
      color: COLORS.orange,
   },

   loginBtn: {
      width: "80%",
      borderRadius: 25,
      marginTop: 30,
      height: 50,
      alignItems: "center",
      justifyContent: "center",
      backgroundColor: COLORS.primary,
   },

   loginText: {
      color: COLORS.white,
      fontSize: 16,
   },
});
