import React, { useState } from "react";
import { StyleSheet, Text, View, Image, TextInput, Button, TouchableOpacity } from "react-native";
import COLORS from "../consts/colors";

export default function ForgotPassword() {
   const [email, setEmail] = useState("");

   return (
      <View style={styles.container}>
         <Image style={styles.image} source={require("../assets/logoCorregidor.png")} />

         <View style={styles.inputView}>
            <TextInput
               style={styles.TextInput}
               placeholder="Email"
               placeholderTextColor={COLORS.grey}
               onChangeText={(email) => setEmail(email)}
            />
         </View>

         <TouchableOpacity style={styles.loginBtn}>
            <Text style={styles.loginText}>Recuperar Contraseña</Text>
         </TouchableOpacity>
      </View>
   );
}

const styles = StyleSheet.create({
   container: {
      flex: 1,
      backgroundColor: "#fff",
      alignItems: "center",
      justifyContent: "center",
   },

   image: {
      width: 280,
      height: 240,
      marginBottom: 40,
   },

   inputView: {
      backgroundColor: COLORS.white,
      borderRadius: 30,
      borderColor: COLORS.grey,
      borderWidth: 1,
      width: "70%",
      height: 45,
      marginBottom: 20,
   },

   TextInput: {
      height: 50,
      flex: 1,
      padding: 10,
      marginLeft: 20,
   },

   forgot_button: {
      height: 30,
      marginTop: 40,
      color: COLORS.orange,
      textDecorationLine: "underline",
   },

   loginBtn: {
      width: "80%",
      borderRadius: 25,
      height: 50,
      alignItems: "center",
      justifyContent: "center",
      marginTop: 32,
      backgroundColor: COLORS.primary,
   },

   loginText: {
      color: COLORS.white,
      fontSize: 16,
   },
   signUpBtn: {
      width: "80%",
      borderRadius: 25,
      height: 50,
      alignItems: "center",
      justifyContent: "center",
      marginTop: 32,
      borderColor: COLORS.orange,
      borderWidth: 1,
   },

   signUpText: {
      color: COLORS.orange,
      fontSize: 16,
   },
});
