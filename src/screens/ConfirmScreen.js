import React, { useState } from "react";
import { StyleSheet, Text, View, Image, TextInput, Button, TouchableOpacity } from "react-native";
import COLORS from "../consts/colors";
import { useAuth } from "../utils/auth";

import logo from "../assets/logoCorregidor.png";

export default function ConfirmSignUp({ navigation }) {
   const { confirmSignUp } = useAuth();
   const [email, setEmail] = useState("");
   const [confirmationCode, setConfirmationCode] = useState("");

   async function confirmSignUpForm() {
      try {
         const formData = {
            username: email,
            code: confirmationCode,
         };

         await confirmSignUp(formData);
         navigation.navigate("SignIn");
      } catch (error) {
         console.log(error);
      }
   }
   return (
      <View style={styles.container}>
         <Image style={styles.image} source={logo} />

         <View style={styles.inputView}>
            <TextInput
               style={styles.TextInput}
               placeholder="Email"
               placeholderTextColor={COLORS.grey}
               onChangeText={setEmail}
            />
         </View>

         <View style={styles.inputView}>
            <TextInput
               style={styles.TextInput}
               placeholder="Confirmation Code"
               placeholderTextColor={COLORS.grey}
               secureTextEntry={true}
               onChangeText={setConfirmationCode}
            />
         </View>

         <TouchableOpacity style={styles.loginBtn} onPress={confirmSignUpForm}>
            <Text style={styles.loginText}>Confirmar Registro</Text>
         </TouchableOpacity>
      </View>
   );
}

const styles = StyleSheet.create({
   container: {
      flex: 1,
      backgroundColor: "#fff",
      alignItems: "center",
      justifyContent: "center",
   },

   image: {
      width: 280,
      height: 240,
      marginBottom: 40,
   },

   inputView: {
      backgroundColor: COLORS.white,
      borderRadius: 30,
      borderColor: COLORS.grey,
      borderWidth: 1,
      width: "70%",
      height: 45,
      marginBottom: 20,
   },

   TextInput: {
      height: 50,
      flex: 1,
      padding: 10,
      marginLeft: 20,
   },

   forgot_button: {
      height: 30,
      marginTop: 40,
      color: COLORS.orange,
      textDecorationLine: "underline",
   },

   loginBtn: {
      width: "80%",
      borderRadius: 25,
      height: 50,
      alignItems: "center",
      justifyContent: "center",
      marginTop: 32,
      backgroundColor: COLORS.primary,
   },

   loginText: {
      color: COLORS.white,
      fontSize: 16,
   },
   signUpBtn: {
      width: "80%",
      borderRadius: 25,
      height: 50,
      alignItems: "center",
      justifyContent: "center",
      marginTop: 32,
      borderColor: COLORS.orange,
      borderWidth: 1,
   },

   signUpText: {
      color: COLORS.orange,
      fontSize: 16,
   },
});
