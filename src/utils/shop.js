import React from "react";
import { createContext, useContext, useState } from "react";

let ShopContext = createContext();

function useShop() {
   return useContext(ShopContext);
}

function ShopProvider({ children }) {
   let [cart, setCart] = useState([]);
   let [total, setTotal] = useState(0);

   function addProduct(item) {
      let newCart = [...cart];
      newCart.push(item);
      setCart(newCart);
      setTotal((prev) => prev + item.price);
   }

   function removeProduct() {}

   function makePayment() {
      setCart([]);
      setTotal(0);
   }

   let value = {
      cart,
      total,
      addProduct,
      removeProduct,
      makePayment,
   };

   return <ShopContext.Provider value={value}>{children}</ShopContext.Provider>;
}

export { useShop, ShopProvider };
