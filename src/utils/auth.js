import React from "react";
import { createContext, useContext, useState, useEffect } from "react";

import { Auth } from "aws-amplify";
// import { createUser, getCurrentUser } from "./actions";

const AuthContext = createContext();

function useAuth() {
   return useContext(AuthContext);
}

function AuthProvider({ children }) {
   let [user, setUser] = useState(null);
   let [isLoading, setIsLoading] = useState(true);

   async function signUp(formData) {
      try {
         await Auth.signUp({
            username: formData.username,
            password: formData.password,
         });
         // await createUser(formData);
      } catch (e) {
         console.log(e);
         throw new Error("Error al crear cuenta");
      }
   }

   async function signIn(formData) {
      try {
         let currentUser = await Auth.signIn(formData.username, formData.password);
         setUser(currentUser);
      } catch {
         throw new Error("Error al iniciar sesión");
      }
   }

   async function signOut() {
      try {
         await Auth.signOut();
         setUser(null);
      } catch {
         throw new Error("Error al cerrar sesión");
      }
   }

   async function confirmSignUp(formData) {
      try {
         await Auth.confirmSignUp(formData.username, formData.code);
      } catch {
         throw new Error("Error al validar cuenta");
      }
   }

   async function forgotPassword(formData) {
      try {
         await Auth.forgotPassword(formData.username);
      } catch {
         throw new Error("Error al enviar correo");
      }
   }

   async function forgotPasswordSubmit(formData) {
      try {
         await Auth.forgotPasswordSubmit(formData.username, formData.code, formData.password);
      } catch {
         throw new Error("Error al cambiar contraseña");
      }
   }

   async function checkAuthState() {
      try {
         let currentUser = await Auth.currentAuthenticatedUser();
         setUser(currentUser);
      } catch {
         setUser(null);
      } finally {
         setIsLoading(false);
      }
   }

   useEffect(function () {
      checkAuthState();
   }, []);

   let value = {
      user,
      isLoading,
      signIn,
      signUp,
      signOut,
      confirmSignUp,
      forgotPassword,
      forgotPasswordSubmit,
   };

   return <AuthContext.Provider value={value}>{children}</AuthContext.Provider>;
}

export { useAuth, AuthProvider };
